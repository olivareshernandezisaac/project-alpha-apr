from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from projects.models import Project
from projects.forms import ProjectForm

# Create your views here.


@login_required
def list_projects(request):
    project_list = Project.objects.filter(owner=request.user) #all the projects shown need for owner to equal the request. so someone else cannot see the projecs of others
    context = {
        "project_list": project_list,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {
        "project_detail": project,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        create_form = ProjectForm(request.POST)
        if create_form.is_valid():
            new_project = create_form.save(False)
            # new_project.owner = request.user ommited this since we want to decide who the will be in charged of this project
            new_project.save()
            return redirect("list_projects")
    else:
        create_form = ProjectForm()
    context = {
        "create_form": create_form,
    }
    return render(request, "projects/create.html", context)
