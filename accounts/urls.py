from django.urls import path
from accounts.views import login_account, logout_account, user_signup

urlpatterns = [
    path("login/", login_account, name="login"),
    path("logout/", logout_account, name="logout"),
    path("signup/", user_signup, name="signup"),
]
