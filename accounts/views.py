from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from accounts.form import AccountForm, SignUpForm
from django.contrib.auth.models import User


# Create your views here.
def login_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect("list_projects")
    else:
        form = AccountForm()
    context = {
        "form_account": form,
    }
    return render(request, "accounts/login.html", context)


def logout_account(request):
    logout(request)
    return redirect("login")


def user_signup(request):
    if request.method == "POST":
        form_signup = SignUpForm(request.POST)
        if form_signup.is_valid():
            username = form_signup.cleaned_data["username"]
            password = form_signup.cleaned_data["password"]
            password_confirmation = form_signup.cleaned_data[
                "password_confirmation"
            ]

            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,
                )
            login(request, user)
            return redirect("list_projects")
        else:
            form_signup.add_error("the passwords do not match")
    else:
        form_signup = SignUpForm()
    context = {
        "form_signup": form_signup,
    }
    return render(request, "accounts/signup.html", context)
