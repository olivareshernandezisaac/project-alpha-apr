from django.shortcuts import render, redirect
from tasks.models import Task
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form_task = TaskForm(request.POST)
        if form_task.is_valid():
            form_task.save()
            return redirect("list_projects")
    else:
        form_task = TaskForm()
    context = {"form_task": form_task}
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    tasks_assigned = Task.objects.filter(assignee=request.user)
    context = {
        "tasks_assigned": tasks_assigned,
    }
    return render(request, "tasks/list.html", context)
